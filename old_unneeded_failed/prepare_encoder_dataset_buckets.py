import cv2
import os

os.environ['OPENCV_IO_ENABLE_JASPER']= "1"

def fill_buckets(video_files, path):
    bucket_count = 1024
    bucket_counter = []
    for i in range(0, bucket_count):
        bucket_counter.append(0)
        directory = path + "/bucket_{0:04d}/".format(i)
        if not os.path.exists(directory):
            os.makedirs(directory)

    bucket_iterator = 0
    for video_file in video_files:
        vidcap = cv2.VideoCapture(video_file)
        print("Processing {0} ..".format(video_file))

        while True:
            success,image = vidcap.read()
            if not success:
                break
            # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            if bucket_iterator == bucket_count:
                bucket_iterator = 0
            cv2.imwrite(path + "/bucket_{0:04d}/{1:06d}.jpg".format(bucket_iterator, bucket_counter[bucket_iterator]), image)
            bucket_counter[bucket_iterator] += 1
            bucket_iterator += 1
        vidcap.release()


path = 'n:/Projects/UCF-101/'

video_files_all = []
# r=root, d=directories, f = files
for r, d, f in os.walk(path):
    for file in f:
        #if '.avi' in file:
        video_files_all.append(os.path.join(r, file))

print("Found {0} videos".format(len(video_files_all)))

counter_train_test = 0
video_files_training = []
video_files_test = []
for video_file in video_files_all:
    if counter_train_test % 10 == 0:
        video_files_test.append(video_file)
    else:
        video_files_training.append(video_file)
    counter_train_test += 1

fill_buckets(video_files_training, "E:/Projects/MVI_sem/encoder/training")
fill_buckets(video_files_test, "E:/Projects/MVI_sem/encoder/test")
