import ctypes
try:
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cudart64_100.dll")
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cudnn64_7.dll")
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cublas64_100.dll")
except BaseException as a:
    print(a)


import cv2
import datetime
import numpy as np
import os
import pickle
from sys import stdout
from time import sleep

def load_binary(path):
  fd = open(path, "rb")
  array = np.frombuffer(fd.read(), dtype='uint8')
  fd.close()
  stdout.write("Processing: " + path + " len: " + str(len(array)) + "\r")
  stdout.flush()
  return array

def decode_img(raw_image):
  image = np.array(raw_image, dtype=np.float32)
  image = image / 255.0
  return image

def write_bucket(path):
    # r=root, d=directories, f = files
    skip_folder = 1000
    buckets = []
    for r, d, f in os.walk(path):
        for bucket_dir in d:
            imgs_0 = []
            imgs_1 = []
            imgs_2 = []
            full_dir = os.path.join(r, bucket_dir)
            print("Loading bucket bucket: " + str(full_dir) + "...")
            for rr, dd, ff in os.walk(full_dir):
              for ddir in dd:
                dir = os.path.join(rr, ddir)
                img_0, img_1, img_2 = load_binary(dir + "/0.jpg"), load_binary(dir + "/1.jpg"), load_binary(dir + "/2.jpg")
                if len(img_0) > 0 and len(img_1) > 0 and len(img_2) > 0:
                  imgs_0.append(img_0)
                  imgs_1.append(img_1)
                  imgs_2.append(img_2)
            print("Finished loading dirs  img_0: {0} img_1: {1} img_2: {2}.".format(len(imgs_0), len(imgs_1), len(imgs_2)))
            array = (imgs_0, imgs_1, imgs_2)
            pickle_path = path + "/{0}_packed.pickle".format(str(bucket_dir))
            print("Saving: " + pickle_path)
            with open(pickle_path, 'wb') as f:
                # Pickle the 'data' dictionary using the highest protocol available.
                pickle.dump(array, f, pickle.HIGHEST_PROTOCOL)
            print("Finished saving.")


write_bucket("d:/Projects/MVI_sem/synth/training/")
write_bucket("d:/Projects/MVI_sem/synth/test/")
