import ctypes
hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cudart64_100.dll")
hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cudnn64_7.dll")


import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import os
import cv2
import random
import tensorflow as tf

import autoencoder

def decode_img(raw_image):
  image = np.array(raw_image, dtype=np.float32)
  image = image / 255.0
  return image

def read_image(path):
    #image = tf.io.read_file(path)
    raw_image = cv2.imread(path)
    raw_image = cv2.resize(raw_image, dsize=autoencoder.Image_Size)
    raw_image = cv2.cvtColor(raw_image, cv2.COLOR_BGR2RGB)
    decoded = decode_img(raw_image)
    return decoded

def read_bucket(path):
    files = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(path):
        for file in f:
          files.append(file)

    random.shuffle(files)

    for file in files:
        yield read_image(os.path.join(r, file))



batch_size = 16
class Dataset_Generator(tf.keras.utils.Sequence) :
  def __init__(self, dir_path) :
    self.files = []
    # r=root, d=directories, f = files
    skip_folder = 123
    counter = 0
    for r, d, f in os.walk(dir_path):
        for dir in d:
          if counter == skip_folder:
            counter = 0
          else:
            counter += 1
            continue

          full_dir = os.path.join(r, dir)
          for rr, dd, ff in os.walk(full_dir):
            for file in ff:
              self.files.append(os.path.join(rr, file))
    


  def __len__(self) :
    return len(self.files) // batch_size

  def __getitem__(self, idx) :
    idx_actual = batch_size * idx
    images = np.empty((batch_size, *autoencoder.Glob_shape))
    batch_it = 0
    for i in range(idx_actual, idx_actual + batch_size):
      if i < len(self.files):
        img = read_image(self.files[i])
        images[batch_it,] = img
        batch_it += 1

    return images, images



#ae = tf.keras.Sequential()
#ae.add(tf.keras.layers.Conv2D(1, (3, 3), activation='relu', input_shape=Glob_shape, padding='same'))
#ae.add(tf.keras.layers.Conv2D(3, (3, 3), activation='sigmoid', padding='same'))

ae = autoencoder.Autoencoder(trainable=True)
# ae.compile(optimizer='adadelta', loss='binary_crossentropy')
#ae.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
ae.compile(optimizer='adam', loss='mse', metrics=['accuracy'])
#ae.build(input_shape=(2, 32, 32, 3))
#ae.summary()

from tensorflow.keras.callbacks import TensorBoard

training_generator = Dataset_Generator("d:/Projects/MVI_sem/encoder/training/")
validation_generator = Dataset_Generator("d:/Projects/MVI_sem/encoder/test/")

#training_generator = Dataset_Generator("X:/Temp/train")
#validation_generator = Dataset_Generator("X:/Temp/train")

checkpoint_path = "encoder_checkpoints/train2.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)

# Create a callback that saves the model's weights
cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                 save_weights_only=True,
                                                 verbose=1)

ae.load_weights(checkpoint_path)

ae.fit_generator(
  generator=training_generator,
  validation_data = validation_generator,
  epochs = 15,
  verbose = 1,
  callbacks=[cp_callback])#, TensorBoard(log_dir='tmp')])

ae.save("ae2.mdl")
