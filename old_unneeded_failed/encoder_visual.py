import ctypes
hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cudart64_100.dll")
hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cudnn64_7.dll")

import cv2
import datetime
import numpy as np
import os
import tensorflow as tf

import matplotlib.pyplot as plt
import matplotlib.image as mpimg

import autoencoder

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

config = ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.2
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)
tf.compat.v1.keras.backend.set_session(session)

def decode_img(raw_image):
  image = np.array(raw_image, dtype=np.float32)
  image = image / 255.0
  return image

ae = tf.keras.models.load_model("ae2.mdl")
ae.build(input_shape=(1, *autoencoder.Glob_shape))
print(ae.layers)

encoder = tf.keras.models.Sequential()
ae.layers[0].learning = False
encoder.add(ae.layers[0])
encoder.build(input_shape=(1, *autoencoder.Glob_shape))

decoder = tf.keras.models.Sequential()
ae.layers[1].learning = False
decoder.add(ae.layers[1])
decoder.build(input_shape=(1, 15, 20, 4))

print(ae.layers[0].layers)

#img_path = 'd:/Projects/MVI_sem/encoder/training/bucket_0000/000000.jpg'
img_path = 'x:/Temp/huehuehue.jpg'


raw_image = cv2.imread(img_path)
raw_image = cv2.resize(raw_image, dsize=autoencoder.Image_Size)
raw_image = cv2.cvtColor(raw_image, cv2.COLOR_BGR2RGB)
decoded = decode_img(raw_image)

input = np.empty((1, *autoencoder.Glob_shape), dtype=np.float32)

input[0, ] = decoded

# output = ae.predict(input)
np.random.seed(6666)
encoded = encoder.predict(input)
encoded[0, 10] = np.random.random((20, 4))
encoded[0, :, 10] = np.random.random((15, 4))
output = decoder.predict(encoded)

cv2.imshow('input',input[0,])
cv2.imshow('output',output[0,])
cv2.waitKey(0)