import ctypes
try:
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cudart64_100.dll")
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cudnn64_7.dll")
except BaseException as a:
    print(a)

import numpy as np
import os
import tensorflow as tf
from tensorflow.keras import layers

Image_Size = (320, 240)
Glob_shape = (Image_Size[1], Image_Size[0], 3)

class Encoder(tf.keras.layers.Layer):
  def __init__(self, trainable):
    super(Encoder, self).__init__()
    self.layers = [
      layers.Conv2D(16, (3, 3), activation='relu', padding='same', trainable = trainable),
      layers.MaxPooling2D((2, 2), padding='same', trainable = trainable),
      layers.Conv2D(16, (3, 3), activation='relu', padding='same', trainable = trainable),
      layers.MaxPooling2D((2, 2), padding='same', trainable = trainable),
      layers.Conv2D(8, (3, 3), activation='relu', padding='same', trainable = trainable),
      layers.MaxPooling2D((2, 2), padding='same', trainable = trainable),
      layers.Conv2D(4, (3, 3), activation='relu', padding='same', trainable = trainable),
      layers.MaxPooling2D((2, 2), padding='same', trainable = trainable)
    ]

  def call(self, input):
    activation = input
    for layer in self.layers:
      activation = layer(activation)
    return activation

class Decoder(tf.keras.layers.Layer):
  def __init__(self, trainable):
    super(Decoder, self).__init__()
    self.hidden_layers = [
      layers.Conv2D(4, (3, 3), activation='relu', padding='same', trainable = trainable),
      layers.UpSampling2D((2, 2)),
      layers.Conv2D(8, (3, 3), activation='relu', padding='same', trainable = trainable),
      layers.UpSampling2D((2, 2)),
      layers.Conv2D(16, (3, 3), activation='relu', padding='same', trainable = trainable),
      layers.UpSampling2D((2, 2)),
      layers.Conv2D(16, (3, 3), activation='relu', padding='same', trainable = trainable),
      layers.UpSampling2D((2, 2)),
      layers.Conv2D(3, (3, 3), activation='sigmoid', padding='same', trainable = trainable)
    ]
  
  def call(self, code):
    activation = code
    for layer in self.hidden_layers:
      activation = layer(activation)
    return activation

class Autoencoder(tf.keras.Model):
  def __init__(self, trainable):
    super(Autoencoder, self).__init__()
    self.encoder = Encoder(trainable)
    self.decoder = Decoder(trainable)

  def call(self, input_features):
    code = self.encoder(input_features)
    code = tf.keras.layers.GaussianNoise(0.3)(code)
    reconstructed = self.decoder(code)
    return reconstructed
