import ctypes
try:
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cudart64_100.dll")
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cudnn64_7.dll")
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cublas64_100.dll")
except BaseException as a:
    print(a)

import cv2
import datetime
import numpy as np
import os
import tensorflow as tf
import zipfile

import matplotlib.pyplot as plt
import matplotlib.image as mpimg

import synth_model

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

model_path = "synth_10.mdl"

def load_model():
  config = ConfigProto()
  config.gpu_options.per_process_gpu_memory_fraction = 0.8
  config.gpu_options.allow_growth = True
  session = InteractiveSession(config=config)
  tf.compat.v1.keras.backend.set_session(session)

  synth = synth_model.build_synth_model_2(False)
  #synth.build(input_shape=synth_model.Img_batch_shape)
  synth.set_weights(tf.keras.models.load_model(model_path).get_weights())
  return synth

def decode_img(raw_image):
  image = np.array(raw_image, dtype=np.float32)
  image = image / 255.0
  return image

def load_img(img_path):
    raw_image = cv2.imread(img_path)
    raw_image = cv2.resize(raw_image, dsize=(synth_model.Img_size[0], synth_model.Img_size[1]))
    #raw_image = cv2.cvtColor(raw_image, cv2.COLOR_BGR2RGB)
    decoded = decode_img(raw_image)
    #decoded = cv2.GaussianBlur(decoded, (5, 5), 0)
    return decoded

path = "example_frames/"
def predict_images(model):
  input_f0 = np.empty((32, *synth_model.Img_shape), dtype=np.float32)
  input_f1 = np.empty((32, *synth_model.Img_shape), dtype=np.float32)
  input_f2 = np.empty((32, *synth_model.Img_shape), dtype=np.float32)

  input_f0[0, ] = load_img(path + "0.jpg")
  input_f1[0, ] = load_img(path + "1.jpg")
  input_f2[0, ] = load_img(path + "2.jpg")

  predicted = model.predict((input_f0, input_f1))


  return (input_f0[0,], input_f1[0,], input_f2[0,], predicted[0,])

def show_images(images):
  cv2.imshow('input 0',images[0])
  cv2.imshow('input 1',images[1])
  cv2.imshow('input 2',images[2])
  cv2.imshow('predicted_2',images[3])

  cv2.waitKey(0)

with zipfile.ZipFile(model_path + ".zip", 'r') as zip_ref:
    zip_ref.extractall("./")

show_images(predict_images(load_model()))