import cv2
import os
import numpy as np

os.environ['OPENCV_IO_ENABLE_JASPER']= "1"

def mse(imageA, imageB):
    err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
    err /= float(imageA.shape[0] * imageA.shape[1])
    return err

def fill_buckets(video_files, path):
    bucket_count = 1024
    bucket_counter = []
    for i in range(0, bucket_count):
        bucket_counter.append(0)
        directory = path + "/bucket_{0:04d}/".format(i)
        if not os.path.exists(directory):
            os.makedirs(directory)


    mmin=1000
    mmax=0
    bucket_iterator = 0
    for video_file in video_files:
        vidcap = cv2.VideoCapture(video_file)
        print("Processing {0} ..".format(video_file))

        success0,image0 = vidcap.read()
        success1,image1 = vidcap.read()

        while True:
            success2,image2 = vidcap.read()
            if not success0 or not success1 or not success2:
                break

            diff_min = min(mse(image0, image1), mse(image1, image2))
#            diff_max = max(mse(image0, image1), mse(image1, image2))

            if diff_min > 350:
                # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                if bucket_iterator == bucket_count:
                    bucket_iterator = 0

                directory = path + "/bucket_{0:04d}/{1:06d}".format(bucket_iterator, bucket_counter[bucket_iterator])
                if not os.path.exists(directory):
                    os.makedirs(directory)

                cv2.imwrite(directory + "/0.jpg", image0)
                cv2.imwrite(directory + "/1.jpg", image1)
                cv2.imwrite(directory + "/2.jpg", image2)
                bucket_counter[bucket_iterator] += 1
                bucket_iterator += 1
            success0,image0 = success1,image1
            success1,image1 = success2,image2
        vidcap.release()


path = 'UCF_SRC/'

video_files_all = []
# r=root, d=directories, f = files
for r, d, f in os.walk(path):
    for file in f:
        #if '.avi' in file:
        video_files_all.append(os.path.join(r, file))

print("Found {0} videos".format(len(video_files_all)))

counter_train_test = 0
video_files_training = []
video_files_test = []
for video_file in video_files_all:
    if counter_train_test % 10 == 0:
        video_files_test.append(video_file)
    else:
        video_files_training.append(video_file)
    counter_train_test += 1

fill_buckets(video_files_training, "UCF_buckets/training")
fill_buckets(video_files_test, "UCF_buckets/test")
