# Zadani
## Extrapolace videa
  Vytvorte model pro extrapolaci snimku videa, ktery muze byt pouzit napr. pro predikci vypadku snimku v realtime renderingu her.


# Zdrojova data
Stahnete https://www.crcv.ucf.edu/data/UCF101.php
Rozbalte do slozky UCF_SRC tak, aby tam jiz byly podslozky jednotlivych kategorii videi (podminka)

## Zavislosti pro jakekoliv spusteni
CUDA v10.0 (pouze pokud bude pouzit tensorflow-gpu - tam pozor, ze je podpora jen jedne konkretni verze)

Python 3.7 (jediny zkouseny)
 - opencv-python
 - numpy
 - tensorflow-gpu (nebo tensorflow-cpu pokud je min nez 8GB VRAM)


# Spusteni pripravy dat a trenovani
Je potreba alespon 150GB mista a vyrazne doporucuji SSD ci velmi rychly HDD. S externim USB3 HDD trvalo zpracovani dat 3 dny kvuli rovnomernemu rozsevani souboru do slozek.

Je potreba dalsi python zavislosti:
 - tensorboard

Jedna se o 3 kroky
 1. Extrakce snimku do bucketu:
    `python prepare_synth_dataset_buckets.py`
 2. Zabaleni bucketu do vetsich balicku:
    `python generate_bucket_blobs_batch.py`
 3. Samotne trenovani:
    - Pokud je malo RAM ( < 32GB RAM), tak je vhodne zvetsit skip_folder v synth_train.py a max offset v train.py (4 odpovida pro 50GB 12GB RAM, ale ve skutecnosti je to aspon 2x tolik)
    `python train.py`


# Spusteni ukazky

Pripraveny final_example.py script sam rozbali model a spusti ukazku na snimcich z example_frames, ktere se zobrazi pomoci 4 oken opencv (3 originalni frame za sebou + 1 predikce posledniho, srovnatelneho s 3.).
Staci spustit:
 `python final_example.py`


Generovani ukazkoveho videa na https://halt.cz/mvi_data/ je zakomentovane v synth_visual.py a je treba upravit cesty)

# Popis souboru
- `final_example.py` - Zjednodusena ukazka jednoho pripadu
- `prepare_synth_dataset_buckets.py` - Vyextrahuje snimky z jednotlivych videi do `UCF_SRC`. Zapisuje jen snimky, ktere maji mezi sebou rozdily.
- `generate_bucket_blobs_batch.py` - Z predpripravenych bucketu vytvori samostatne soubory pro rychlejsi cteni z FS. Cte z `UCF_SRC` a zapisuje do `UCF_buckets`
- `synth_10.mdl.zip` - Zabaleny model
- `synth_model.py` - Deklarace sekvencniho keras modelu.
- `synth_train.py` - Spusteni trenovani s offsetem slozek (pro vyber ruznych datasetu), ktere se nevejdou do RAM + priznak, zda-li navazat na predchozi beh, napr. `python synth.train 0 1` - spusti nove trenovani pro prvni dataset
- `synth_visual.py` - Vizualizace + debug kod pro generovani videi
- `train.py` - Spusteni samotneho trenovani, ktery ma zahardcodovany pocet datasetu (urceny pomoci skip_folder uvnitr synth_train.py) - sam spousti `synth.train`
- `utils.py` - Upraveny zkopirovany kod pouzity v Deep Voxel Flow. Obsahuje vrstvu pro samplovani textury pomoci koordinatu, ktera umoznuje beznou zpetnou propagaci + generovani rovnomerne mrizky 0-1, pro navedeni site do alespon trivialni identity.
  