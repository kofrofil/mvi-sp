import ctypes
try:
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cudart64_100.dll")
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cudnn64_7.dll")
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cublas64_100.dll")
except BaseException as a:
    print(a)

import cv2
import datetime
import numpy as np
import os
import tensorflow as tf

import matplotlib.pyplot as plt
import matplotlib.image as mpimg

import synth_model

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

def load_model():
  config = ConfigProto()
  config.gpu_options.per_process_gpu_memory_fraction = 0.8
  config.gpu_options.allow_growth = True
  session = InteractiveSession(config=config)
  tf.compat.v1.keras.backend.set_session(session)

  synth = synth_model.build_synth_model_2(False)
  #synth.build(input_shape=synth_model.Img_batch_shape)
  synth.set_weights(tf.keras.models.load_model("synth_10.mdl").get_weights())
  return synth

def decode_img(raw_image):
  image = np.array(raw_image, dtype=np.float32)
  image = image / 255.0
  return image




#img_path = 'd:/Projects/MVI_sem/encoder/training/bucket_0000/000000.jpg'
#path = 'x:/Temp/huehuehue.jpg'
#path = "d:/Projects/MVI_sem/synth/training/bucket_0001/000000/"
#path = "d:/Projects/MVI_sem/synth_large/training/bucket_0100/000494/"
#path = "d:/Projects/MVI_sem/synth_large/training/bucket_0100/000531/"
#path = "d:/Projects/MVI_sem/synth_large/training/bucket_0113/000014/"
#path = "d:/Projects/MVI_sem/synth_large/training/bucket_0113/000069/"
#path = "d:/Projects/MVI_sem/synth_large/training/bucket_0113/000319/" # really good
path = "d:/Projects/MVI_sem/synth_large/training/bucket_0113/000589/" # really bad
#path = "d:/Projects/MVI_sem/synth_large/training/bucket_0113/000609/" # wtf
#path = "d:/Projects/MVI_sem/synth_large/training/bucket_0113/000614/"
#path = "x:/Others/SyncThing/Filip/School/MI-MVI/Semestral/compar/"

vid_path = "v_Skiing_g25_c01.avi"

def load_img(img_path):
    raw_image = cv2.imread(img_path)
    raw_image = cv2.resize(raw_image, dsize=(synth_model.Img_size[0], synth_model.Img_size[1]))
    #raw_image = cv2.cvtColor(raw_image, cv2.COLOR_BGR2RGB)
    decoded = decode_img(raw_image)
    #decoded = cv2.GaussianBlur(decoded, (5, 5), 0)
    return decoded


def predict_images(model):
  input_f0 = np.empty((32, *synth_model.Img_shape), dtype=np.float32)
  input_f1 = np.empty((32, *synth_model.Img_shape), dtype=np.float32)
  input_f2 = np.empty((32, *synth_model.Img_shape), dtype=np.float32)

  input_f0[0, ] = load_img(path + "0.jpg")
  input_f1[0, ] = load_img(path + "1.jpg")
  input_f2[0, ] = load_img(path + "2.jpg")

  # output = ae.predict(input)
  #np.random.seed(6666)
  #print(input_f0.shape)
  #print(input_f1.shape)
  predicted = model.predict((input_f0, input_f1))

  """
  out = cv2.VideoWriter('wtf.avi',cv2.VideoWriter_fourcc(*'DIVX'), 10, (320, 240))
  f0 = input_f0
  f1 = input_f1
  print(f0.shape)
  for i in range(0, 120):
    print("Frame {0}".format(i))
    curr = model.predict((f0, f1))
    f0, f1 = f1, curr
    image = curr[0,] * 255.0
    image = np.array(image, dtype=np.uint8)
    out.write(image)
  out.release()
  """

  """
  vidcap = cv2.VideoCapture(vid_path)
  out = cv2.VideoWriter('processed_double_fps.avi',cv2.VideoWriter_fourcc(*'DIVX'), vidcap.get(cv2.CAP_PROP_FPS) * 2, (320, 240))

  success0,image0 = vidcap.read()
  input_f0[0, ] = decode_img(image0)
  frid=0
  while True:
    success1,image1 = vidcap.read()
    if not success0 or not success1:
        break
    input_f1[0, ] = decode_img(image1)
    processed = model.predict((input_f0, input_f1))
    input_f0[0, ] = input_f1[0, ]
    image = input_f1[0,] * 255.0
    image = np.array(image, dtype=np.uint8)
    out.write(image)
    image = processed[0,] * 255.0
    image = np.array(image, dtype=np.uint8)
    out.write(image)
    print("Processed frame " + str(frid))
    frid = frid + 1
  out.release()
  """

  return (input_f0[0,], input_f1[0,], input_f2[0,], predicted[0,])

def show_images(images):
  cv2.imshow('input 0',images[0])
  cv2.imshow('input 1',images[1])
  cv2.imshow('input 2',images[2])
  cv2.imshow('predicted_2',images[3])

  cv2.imwrite("example_0.jpg", images[0] * 255)
  cv2.imwrite("example_1.jpg", images[1] * 255)
  cv2.imwrite("example_2.jpg", images[2] * 255)
  cv2.imwrite("predicted.jpg", images[3] * 255)

  cv2.waitKey(0)

show_images(predict_images(load_model()))