import ctypes
try:
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cudart64_100.dll")
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cudnn64_7.dll")
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cublas64_100.dll")
except BaseException as a:
    print(a)


import cv2
import datetime
import numpy as np
import os
import pickle
from sys import stdout
from time import sleep

def load_binary(path):
  fd = open(path, "rb")
  array = np.frombuffer(fd.read(), dtype='uint8')
  fd.close()
  stdout.write("Processing: " + path + " len: " + str(len(array)) + "\r")
  stdout.flush()
  return array

def decode_img(raw_image):
  image = np.array(raw_image, dtype=np.float32)
  image = image / 255.0
  return image

def write_buckets(buckets, offset, output_dir):
  min_len = len(buckets[0])
  per_bucket= []
  for i in range(0, len(buckets)):
    min_len = min(len(buckets[i]), min_len)
    per_bucket.append(([], [], []))
  
  for i in range(0, min_len):
    for bi in range(len(buckets)):
      imgs_0, imgs_1, imgs_2 = per_bucket[bi]
      dir = buckets[bi][i]
      img_0, img_1, img_2 = load_binary(dir + "/0.jpg"), load_binary(dir + "/1.jpg"), load_binary(dir + "/2.jpg")
      if len(img_0) > 0 and len(img_1) > 0 and len(img_2) > 0:
        imgs_0.append(img_0)
        imgs_1.append(img_1)
        imgs_2.append(img_2)
  for bi in range(0, len(buckets)):
    pickle_path = output_dir + '/{0:06d}_packed.pickle'.format(offset + bi)
    print("Saving: " + pickle_path)
    with open(pickle_path, 'wb') as f:
      pickle.dump(per_bucket[bi], f, pickle.HIGHEST_PROTOCOL)
      print("Finished saving.")
  

def write_all_buckets(path):
    # r=root, d=directories, f = files
    skip_folder = 1000
    buckets = []
    for r, d, f in os.walk(path):
        for bucket_dir in d:
            imgs_0 = []
            imgs_1 = []
            imgs_2 = []
            full_dir = os.path.join(r, bucket_dir)
            print("Loading bucket bucket: " + str(full_dir) + "...")
            dirs = []
            for rr, dd, ff in os.walk(full_dir):
              for ddir in dd:
                dir = os.path.join(rr, ddir)
                dirs.append(dir)
              break
            buckets.append(dirs)
        break
    batch = 64
    for i in range(0, (len(buckets) // batch)):
      temp_buckets = []
      for bi in range(i * batch, (i + 1) * batch):
        temp_buckets.append(buckets[bi])
      write_buckets(temp_buckets, i * batch, path)

write_all_buckets("UCF_buckets/training/")
write_all_buckets("UCF_buckets/test/")
