import ctypes
try:
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cudart64_100.dll")
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cudnn64_7.dll")
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cublas64_100.dll")
except BaseException as a:
    print(a)


import cv2
import datetime
import numpy as np
import os
import pickle
import tensorflow as tf
from tensorflow.keras import layers
import sys

import synth_model

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

import tempfile

#config = ConfigProto()
#config.gpu_options.per_process_gpu_memory_fraction = 0.8
#config.gpu_options.allow_growth = True
#session = InteractiveSession(config=config)
#tf.compat.v1.keras.backend.set_session(session)


def load_binary(path):
  fd = open(path, "rb")
  array = np.frombuffer(fd.read(), dtype='uint8')
  fd.close()
  return array

def decode_img(raw_image):
  image = np.array(raw_image, dtype=np.float32)
  image = image / 255.0
  return image

def read_image(path):
    raw_image = cv2.imread(path)
    raw_image = cv2.resize(raw_image, (synth_model.Img_size[0], synth_model.Img_size[1]))
    raw_image = cv2.cvtColor(raw_image, cv2.COLOR_BGR2RGB)
    decoded = decode_img(raw_image)
    return decoded

def read_bin_image(bin):
    raw_image = cv2.imdecode(bin, cv2.IMREAD_UNCHANGED)
    raw_image = cv2.resize(raw_image, (synth_model.Img_size[0], synth_model.Img_size[1]))
    raw_image = cv2.cvtColor(raw_image, cv2.COLOR_BGR2RGB)
    raw_image = cv2.GaussianBlur(raw_image, (3, 3), 0)
    decoded = decode_img(raw_image)
    return decoded

def read_bucket(path):
    dirs = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(path):
        for dir in d:
          dirs.append(dir)

    random.shuffle(dirs)

    for dir in dirs:
        path = os.path.join(r, dir)
        img_0 = read_image(path + "/0.jpg")
        img_1 = read_image(path + "/1.jpg")
        img_2 = read_image(path + "/2.jpg")
        yield (img_0, img_1, img_2)

batch_size = 32
class Dataset_Generator(tf.keras.utils.Sequence) :

  def __init__(self, dir_path) :
    print("Loading dirs at " + dir_path)
    self.imgs_0 = []
    self.imgs_1 = []
    self.imgs_2 = []
    # r=root, d=directories, f = files
    skip_folder = 1
    counter = int(os.sys.argv[1])
    for r, d, f in os.walk(dir_path):
        for bucket_file in f:
          if counter == skip_folder:
            counter = 0
          else:
            counter = counter + 1
            continue

          full_path = os.path.join(r, bucket_file)
          sys.stdout.write("bucket: " + full_path + "\r")
          sys.stdout.flush()
          with open(full_path, 'rb') as f:
            imgs_0, imgs_1, imgs_2 = pickle.load(f)
            min_len = min(min(len(imgs_0), len(imgs_1)), len(imgs_2))
            for i in range(0, min_len):
              img_0, img_1, img_2 = imgs_0[i], imgs_1[i], imgs_2[i]
              if len(img_0) > 0 and len(img_1) > 0 and len(img_2) > 0:
                self.imgs_0.append(img_0)
                self.imgs_1.append(img_1)
                self.imgs_2.append(img_2)
        break
    print("Finished loading dirs  img_0: {0} img_1: {1} img_2: {2}.".format(len(self.imgs_0), len(self.imgs_1), len(self.imgs_2)))


  def __len__(self) :
    return min(min(len(self.imgs_0), len(self.imgs_1)), len(self.imgs_2)) // batch_size
    #return 256 # TODO: Remove

  def __getitem__(self, idx) :
    idx_actual = batch_size * idx
    # hack
    #idx_actual = 492 # TODO: Remove
    images_f0 = np.empty((batch_size, *synth_model.Img_shape))
    images_f1 = np.empty((batch_size, *synth_model.Img_shape))
    images_f2 = np.empty((batch_size, *synth_model.Img_shape))
    batch_it = 0
    for i in range(idx_actual, idx_actual + batch_size):
      if i < len(self.imgs_0):
        images_f0[batch_it,] = read_bin_image(self.imgs_0[i])
        images_f1[batch_it,] = read_bin_image(self.imgs_1[i])
        images_f2[batch_it,] = read_bin_image(self.imgs_2[i])
        batch_it += 1

    return (images_f0, images_f1), images_f2
    #return (images_f2, images_f1), images_f0

model = synth_model.build_synth_model_2(True)
model.build(input_shape=synth_model.Img_batch_shape)
#model.summary()
#print(model)

#os.system("pause")

#opt = tf.keras.optimizers.Adam(lr=0.003)
opt = tf.keras.optimizers.Adam(lr=0.000015)
#opt = tf.keras.optimizers.SGD(lr=0.01, momentum=0.1, decay=0.7)
model.compile(optimizer=opt, loss='mae', metrics=['accuracy'])
#model.summary()

training_dataset_gen = Dataset_Generator("UCF_buckets/training")
validation_dataset_gen = Dataset_Generator("UCF_buckets/test")

checkpoint_path = "synth_checkpoints/train_10.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)

# Create a callback that saves the model's weights
cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                 save_weights_only=True,
                                                 verbose=1)

#class MyCustomCallback(tf.keras.callbacks.Callback):
#  def on_train_batch_end(self, batch, logs=None):

if not int(os.sys.argv[2]) == 1:
  print("Loading previous weights from " + checkpoint_path)
  model.load_weights(checkpoint_path)

tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir='logs', histogram_freq=0,
                          write_graph=True, write_images=False)

model.fit_generator(
  generator=training_dataset_gen,
  validation_data = validation_dataset_gen,
  epochs = 2,
  verbose = 1,
  callbacks=[cp_callback, tensorboard_callback])

model.save("synth_10.mdl")

# TODO: Change initializer to 0
# TODO: Try work with the regularizer

