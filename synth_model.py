import ctypes
try:
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cudart64_100.dll")
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cudnn64_7.dll")
    hllDll = ctypes.WinDLL("C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v10.0\\bin\\cublas64_100.dll")
except BaseException as a:
    print(a)

import cv2
import datetime
import numpy as np
import os
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.regularizers import l2

import utils

Img_size = (320, 240, 3)
Img_shape = (Img_size[1], Img_size[0], 3)
Img_batch_shape = (1, Img_size[1], Img_size[0], 3)

"""
def build_synth_model(training):
    input_f0 = layers.Input(shape=Img_shape)
    input_f1 = layers.Input(shape=Img_shape)

    x = tf.keras.layers.concatenate([input_f0, input_f1], 3)
    x_a = layers.Conv2D(8, (5, 5), activation='relu', padding='same')(x)
    x = layers.MaxPooling2D((2, 2), padding='same')(x)
    x_b = layers.Conv2D(16, (5, 5), activation='relu', padding='same')(x)
    x = layers.MaxPooling2D((2, 2), padding='same')(x)
    x_c = layers.Conv2D(64, (5, 5), activation='relu', padding='same')(x)
    x = layers.MaxPooling2D((2, 2), padding='same')(x)
    x_d = layers.Conv2D(128, (5, 5), activation='relu', padding='same')(x)
    x = layers.MaxPooling2D((2, 2), padding='same')(x)
    x = layers.Conv2D(128, (5, 5), activation='relu', padding='same')(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = layers.UpSampling2D((2, 2))(x)
    x = tf.keras.layers.concatenate([x, x_d], 3)
    x = layers.Conv2D(128, (5, 5), activation='relu', padding='same')(x)
    x = layers.UpSampling2D((2, 2))(x)
    x = tf.keras.layers.concatenate([x, x_c], 3)
    x = layers.Conv2D(64, (5, 5), activation='relu', padding='same')(x)
    x = layers.UpSampling2D((2, 2))(x)
    x = tf.keras.layers.concatenate([x, x_b], 3)
    x = layers.Conv2D(32, (5, 5), activation='relu', padding='same')(x)
    x = layers.UpSampling2D((2, 2))(x)
    x = tf.keras.layers.concatenate([x, x_a], 3)
    x = layers.Conv2D(16, (5, 5), activation='relu', padding='same')(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = layers.Conv2D(2, (5, 5), activation='tanh', padding='same')(x)

    grid_x, grid_y = utils.meshgrid(Img_size[1], Img_size[0])
    grid_x = tf.tile(grid_x, [32, 1, 1]) # batch_size = 32
    grid_y = tf.tile(grid_y, [32, 1, 1]) # batch_size = 32

    x = 0.5 * x

    coor_x_1 = grid_x + x[:, :, :, 0]
    coor_y_1 = grid_y + x[:, :, :, 1]
    x = utils.bilinear_interp(input_f1, coor_x_1, coor_y_1)
    return tf.keras.Model(inputs=[input_f0, input_f1], outputs=x)
"""

def build_synth_model_2(training):
    input_f0 = layers.Input(shape=Img_shape)
    input_f1 = layers.Input(shape=Img_shape)

    x = tf.keras.layers.concatenate([input_f0, input_f1], 3)
    x_a = layers.Conv2D(8, (5, 5), activation='relu', padding='same')(x)
    x_a = tf.keras.layers.BatchNormalization()(x_a)
    x = layers.MaxPooling2D((2, 2), padding='same')(x_a)
    x_b = layers.Conv2D(16, (5, 5), activation='relu', padding='same')(x)
    x_b = tf.keras.layers.BatchNormalization()(x_b)
    x = layers.MaxPooling2D((2, 2), padding='same')(x_b)
    x_c = layers.Conv2D(64, (5, 5), activation='relu', padding='same')(x)
    x_c = tf.keras.layers.BatchNormalization()(x_c)
    x = layers.MaxPooling2D((2, 2), padding='same')(x_c)
    x_d = layers.Conv2D(128, (5, 5), activation='relu', padding='same')(x)
    x_d = tf.keras.layers.BatchNormalization()(x_d)
    x = layers.MaxPooling2D((2, 2), padding='same')(x_d)
    x = layers.Conv2D(128, (5, 5), activation='relu', padding='same')(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = layers.UpSampling2D((2, 2))(x)
    x = tf.keras.layers.concatenate([x, x_d], 3)
    x = layers.Conv2D(128, (5, 5), activation='relu', padding='same')(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = layers.UpSampling2D((2, 2))(x)
    x = tf.keras.layers.concatenate([x, x_c], 3)
    x = layers.Conv2D(64, (5, 5), activation='relu', padding='same')(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = layers.UpSampling2D((2, 2))(x)
    x = tf.keras.layers.concatenate([x, x_b], 3)
    x = layers.Conv2D(32, (5, 5), activation='relu', padding='same')(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = layers.UpSampling2D((2, 2))(x)
    x = tf.keras.layers.concatenate([x, x_a], 3)
    x = layers.Conv2D(16, (5, 5), activation='relu', padding='same')(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = layers.Conv2D(2, (5, 5), activation='tanh', padding='same')(x)

    grid_x, grid_y = utils.meshgrid(Img_size[1], Img_size[0])
    grid_x = tf.tile(grid_x, [32, 1, 1]) # batch_size = 32
    grid_y = tf.tile(grid_y, [32, 1, 1]) # batch_size = 32

    x = 0.5 * x

    coor_x_1 = grid_x + x[:, :, :, 0]
    coor_y_1 = grid_y + x[:, :, :, 1]
    x = utils.bilinear_interp(input_f1, coor_x_1, coor_y_1)
    return tf.keras.Model(inputs=[input_f0, input_f1], outputs=x)

#model = build_synth_model()
#model.build(input_shape=Img_batch_shape)
#model.summary()